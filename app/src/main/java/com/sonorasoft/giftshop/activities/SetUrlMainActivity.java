package com.sonorasoft.giftshop.activities;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import com.sonorasoft.giftshop.GiftShopApplication;
import com.sonorasoft.giftshop.R;
import com.sonorasoft.giftshop.controllers.ApiController;
import com.sonorasoft.giftshop.databinding.ActivitySetUrlMainBinding;
import com.sonorasoft.giftshop.utils.Utils;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class SetUrlMainActivity extends AppCompatActivity {

    private ActivitySetUrlMainBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_set_url_main);

        binding = DataBindingUtil.setContentView(this, R.layout.activity_set_url_main);

        setTitle("Configurar url base");
        setSupportActionBar(binding.toolbar);
        getSupportActionBar().setTitle("Configurar url base");

        binding.edUrl.setText(GiftShopApplication.BASE_URL_SERVICES);

        binding.btnSetUrl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    String url = binding.edUrl.getText().toString();
                    GiftShopApplication.BASE_URL_SERVICES = url;
                    ApiController.init(GiftShopApplication.BASE_URL_SERVICES);
                    SetUrlMainActivity.this.finish();
                }catch (Exception e){
                    Utils.showDialogError(SetUrlMainActivity.this, "Error en configuración", e.getMessage());
                }
            }
        });
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }
}
