package com.sonorasoft.giftshop.activities;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.sonorasoft.giftshop.GiftShopApplication;
import com.sonorasoft.giftshop.R;
import com.sonorasoft.giftshop.database.Database;
import com.sonorasoft.giftshop.databinding.ActivityLoginBinding;
import com.sonorasoft.giftshop.models.LoginResponse;
import com.sonorasoft.giftshop.receivers.LoginReceiver;
import com.sonorasoft.giftshop.receivers.NetworkStatusReceiver;
import com.sonorasoft.giftshop.services.CallService;
import com.sonorasoft.giftshop.services.LoginService;
import com.sonorasoft.giftshop.utils.Helpers;
import com.sonorasoft.giftshop.utils.Utils;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class LoginActivity extends AppCompatActivity implements
        NetworkStatusReceiver.Callback,
        LoginReceiver.Callback {

    private ActivityLoginBinding binding;

    private NetworkStatusReceiver networkStatusReceiver;
    private LoginReceiver loginReceiver;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        binding = DataBindingUtil.setContentView(this, R.layout.activity_login);

        this.loginReceiver = new LoginReceiver(this);
        this.networkStatusReceiver = new NetworkStatusReceiver(this);

        this.loginReceiver.register(this);

        setTitle("Iniciar sesión");
        setSupportActionBar(binding.toolbar);
        getSupportActionBar().setTitle("Iniciar sesión");

        binding.btnEnterLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(validateFields()){
                    requestLogin();
                }
            }
        });
        
        binding.btnergistro.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Toast.makeText(LoginActivity.this, "Registro de usuario", Toast.LENGTH_SHORT).show();
                startActivity(new Intent(LoginActivity.this, RegisterActivity.class));
            }
        });

        binding.btnUrlBase.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(LoginActivity.this, SetUrlMainActivity.class));
            }
        });

        binding.btnIrAtienda.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(LoginActivity.this, MainActivity.class));
                finish();
            }
        });

        if(GiftShopApplication.isSessionActive()){
            startActivity(new Intent(this, MainActivity.class));
            this.finish();
        }
    }


    public boolean validateFields(){

        String correo = binding.edEmail.getText().toString().trim();
        if(correo.isEmpty()){
            Utils.showDialogError(this, "Error", "Ingrese un correo electrónico");
            return false;
        }

        if(!Helpers.validateEmailFormat(correo)){
            Utils.showDialogError(this, "Error", "Ingrese un correo con formato válido");
            return false;
        }

        String contrasenia = binding.edContrasenia.getText().toString();
        if(contrasenia.isEmpty()){
            Utils.showDialogError(this, "Error", "Ingrese una contraseña");
            return false;
        }


        return true;
    }

    @Override
    public void onDestroy() {
        this.loginReceiver.unregister(this);
        super.onDestroy();
    }

    @Override
    public void onResume() {
        super.onResume();
        networkStatusReceiver.register(this);
    }

    @Override
    public void onPause() {
        networkStatusReceiver.unregister(this);
        super.onPause();
    }

    @Override
    public void onNetworkStatusNoInternet() {
        showLoading(false);
        Snackbar.make(binding.getRoot(), "Revise que tenga una conexión activa a internet", Snackbar.LENGTH_LONG).show();
    }

    @Override
    public void onNetworkStatusFailComunicationWs() {
        showLoading(false);
        Snackbar.make(binding.getRoot(), "Error de comunicación con el servidor", Snackbar.LENGTH_LONG).show();
    }

    @Override
    public void onLoginSuccess(LoginResponse loginResponse) {
        Database.saveLogin(loginResponse);
        GiftShopApplication.setSessionActive(true);

        startActivity(new Intent(this, MainActivity.class));
        this.finish();
    }

    @Override
    public void onLoginFailure(String message, int code) {
        showLoading(false);
        Utils.showDialogError(this, "Error al iniciar sesión", message);
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    public void requestLogin(){
        showLoading(true);

        String email = binding.edEmail.getText().toString().trim();
        String password = binding.edContrasenia.getText().toString();

        CallService.execute(this, LoginService.getInstanceService(this, email, password));
    }


    private void showLoading(final boolean show){
        int shortAnimTime = getResources().getInteger(
                android.R.integer.config_mediumAnimTime);

        binding.loginStatus.animate().setDuration(shortAnimTime)
                .alpha(show ? 1 : 0)
                .setListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        binding.loginStatus.setVisibility(show ? View.VISIBLE
                                : View.GONE);
                    }
                });

        binding.mainContainLogin.animate().setDuration(shortAnimTime)
                .alpha(show ? 0 : 1)
                .setListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        binding.mainContainLogin.setVisibility(show ? View.GONE
                                : View.VISIBLE);
                    }
                });
    }
}
