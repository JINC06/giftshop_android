package com.sonorasoft.giftshop.activities;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.net.Uri;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.sonorasoft.giftshop.GiftShopApplication;
import com.sonorasoft.giftshop.R;
import com.sonorasoft.giftshop.adapters.ProductsAdapter;
import com.sonorasoft.giftshop.database.Database;
import com.sonorasoft.giftshop.databinding.ActivityMainBinding;
import com.sonorasoft.giftshop.fragments.dialogs.FilterDialogFragment;
import com.sonorasoft.giftshop.models.Category;
import com.sonorasoft.giftshop.models.GetProductsResponse;
import com.sonorasoft.giftshop.models.Product;
import com.sonorasoft.giftshop.receivers.GetCategoriesReceiver;
import com.sonorasoft.giftshop.receivers.GetProductsReceiver;
import com.sonorasoft.giftshop.receivers.NetworkStatusReceiver;
import com.sonorasoft.giftshop.services.CallService;
import com.sonorasoft.giftshop.services.GetCategoriesService;
import com.sonorasoft.giftshop.services.GetProductsService;
import com.sonorasoft.giftshop.utils.Utils;

import java.util.ArrayList;
import java.util.List;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class MainActivity extends AppCompatActivity implements
        NetworkStatusReceiver.Callback,
        GetCategoriesReceiver.Callback,
        GetProductsReceiver.Callback,
        FilterDialogFragment.OnFragmentInteractionListener {

    ActivityMainBinding binding;

    //Busqueda
    public static String searchQuery = "";
    public static int idCategory = 0;
    public static int currentPage = 1;
    public static int totalPages = 1;
    public static int perPage = 5;
    public static int totalResults = 0;
    public static String[] valuesPerPages = { "5", "10", "15", "20", "25" };
    public static List<Category> categoryList = new ArrayList<>();
    public static List<Product> products = null;
    public static ProductsAdapter productsAdapter = null;

    private NetworkStatusReceiver networkStatusReceiver;
    private GetCategoriesReceiver getCategoriesReceiver;
    private GetProductsReceiver getProductsReceiver;

    LinearLayoutManager mLayoutManager;
    private boolean loading = true;
    int pastVisiblesItems, visibleItemCount, totalItemCount;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        binding = DataBindingUtil.setContentView(this, R.layout.activity_main);

        this.networkStatusReceiver = new NetworkStatusReceiver(this);
        this.getCategoriesReceiver = new GetCategoriesReceiver(this);
        this.getProductsReceiver = new GetProductsReceiver(this);

        this.getCategoriesReceiver.register(this);
        this.getProductsReceiver.register(this);

        setTitle("Tienda");
        setSupportActionBar(binding.toolbar);
        getSupportActionBar().setTitle("Tienda");

        mLayoutManager = new LinearLayoutManager(this);
        binding.recyclerProducts.setLayoutManager(mLayoutManager);
        
        binding.fabNewProduct.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(MainActivity.this, "Tu eres un ADMIN pero esta función no esta creada aún", Toast.LENGTH_LONG).show();
            }
        });


        binding.recyclerProducts.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                if(dy > 0) //check for scroll down
                {
                    visibleItemCount = mLayoutManager.getChildCount();
                    totalItemCount = mLayoutManager.getItemCount();
                    pastVisiblesItems = mLayoutManager.findFirstVisibleItemPosition();

                    if (loading)
                    {
                        if ( (visibleItemCount + pastVisiblesItems) >= totalItemCount)
                        {
                            loading = false;
                            Log.v("...", "Last Item Wow !");
                            //Do pagination.. i.e. fetch new data
                            int nextPage = currentPage + 1;

                            if(nextPage <= totalPages){
                                currentPage++;
                                binding.progrerssCrecycler.setVisibility(View.VISIBLE);
                                CallService.execute(MainActivity.this, GetProductsService.getInstanceService(MainActivity.this, idCategory, searchQuery, perPage, currentPage));
                            }

                        }
                    }
                }
            }
        });

        invalidateOptionsMenu();

        getProductsService(true);
        getCategoriesService();
    }

    private void getProductsService(boolean first) {
        if(first)
            setView(0);

        CallService.execute(this, GetProductsService.getInstanceService(this, idCategory, searchQuery, perPage, currentPage));
    }

    private void getCategoriesService() {
        CallService.execute(this, GetCategoriesService.getInstanceService(this));
    }

    @Override
    public void onDestroy() {
        this.getCategoriesReceiver.unregister(this);
        this.getProductsReceiver.unregister(this);
        super.onDestroy();
    }

    @Override
    public void onPause() {
        networkStatusReceiver.unregister(this);
        super.onPause();
    }

    @Override
    protected void onResume() {
        super.onResume();
        validateAdmin();
 
        networkStatusReceiver.register(this);
    }

    private void validateAdmin(){
        if(binding != null){
            binding.btnGoShoppingCart.setVisibility(View.GONE);

            if(GiftShopApplication.isSessionActive() && Database.getCurrentSession().getIsAdmin() == 1)
                binding.fabNewProduct.setVisibility(View.VISIBLE);
            else
                binding.fabNewProduct.setVisibility(View.GONE);
        }
    }

    private void setView(int typeView){
        if(typeView == 0){ //progress
            binding.recyclerProducts.setVisibility(View.GONE);
            binding.progressBar.setVisibility(View.VISIBLE);
        }else if(typeView == 1){ //recycler
            binding.recyclerProducts.setVisibility(View.VISIBLE);
            binding.progressBar.setVisibility(View.GONE);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();

        if (GiftShopApplication.isSessionActive()) {
            inflater.inflate(R.menu.menu_search_logout, menu);
        } else {
            inflater.inflate(R.menu.menu_search, menu);
        }

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.action_search:
                showFilter();
                return true;
            case R.id.action_log_out:
                logOut();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void showFilter() {
        FilterDialogFragment selectDialogFragment = new FilterDialogFragment();
        //Bundle bundle = new Bundle();
        //bundle.putString(SelectDialogFragment.TITLE, "Seleccione el vecino a visitar");
        //bundle.putSerializable(SelectDialogFragment.LIST_DATA, (Serializable) neighboursData);
        //selectDialogFragment.setArguments(bundle);
        selectDialogFragment.setmListener(this);
        selectDialogFragment.show(getSupportFragmentManager(), FilterDialogFragment.TAG);
    }

    public void logOut(){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("¿Desea cerrar sesión?");
        builder.setCancelable(true);

        builder.setPositiveButton("SI",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                        Database.deleteLogin();
                        GiftShopApplication.setSessionActive(false);

                        validateAdmin();
                        invalidateOptionsMenu();

                        //finish();
                        //startActivity(new Intent(MainActivity.this, MainActivity.class));
                    }
                });

        builder.setNegativeButton("NO",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });

        AlertDialog alert = builder.create();
        alert.show();
    }

    @Override
    public void onNetworkStatusNoInternet() {
        setView(1);
        Snackbar.make(binding.getRoot(), "Revise que tenga una conexión activa a internet", Snackbar.LENGTH_LONG).show();
    }

    @Override
    public void onNetworkStatusFailComunicationWs() {
        setView(1);
        Snackbar.make(binding.getRoot(), "Error de comunicación con el servidor", Snackbar.LENGTH_LONG).show();
    }

    GetProductsResponse getProductsResponse;
    @Override
    public void onGetProductsSuccess(GetProductsResponse results) {
        setView(1);

        this.getProductsResponse = results;

        if(products == null || products.size() == 0) {

            totalResults = Integer.parseInt(results.getTotalResults());
            perPage = Integer.parseInt(results.getPerPage());
            totalPages = Integer.parseInt(results.getPages());

            products = results.getResults();
            productsAdapter = new ProductsAdapter(this.getLayoutInflater(), products, this);
            binding.recyclerProducts.setAdapter(productsAdapter);

        }else{
            binding.progrerssCrecycler.setVisibility(View.GONE);

            totalResults = Integer.parseInt(results.getTotalResults());
            perPage = Integer.parseInt(results.getPerPage());
            totalPages = Integer.parseInt(results.getPages());

            products.addAll(results.getResults());
            if(productsAdapter != null){

                productsAdapter.setData(products);
                productsAdapter.notifyDataSetChanged();
                loading = true;

            }else{
                products = results.getResults();
                productsAdapter = new ProductsAdapter(this.getLayoutInflater(), products, this);
                binding.recyclerProducts.setAdapter(productsAdapter);
            }

        }

        //set adapter
    }

    @Override
    public void onGetProductsFailure(String message, int code) {
        setView(1);
        Utils.showDialogError(this, "Error al consultar los productos", message);
        loading = true;
    }

    @Override
    public void onGetCategoriesSuccess(List<Category> categories) {
        categoryList = categories;
    }

    @Override
    public void onGetCategoriesFailure(String message, int code) {
        //TODO
    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }

    @Override
    public void searchCriteria() {
        products = null;
        productsAdapter = null;
        loading = true;
        getProductsService(true);
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }
}
