package com.sonorasoft.giftshop.activities;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import com.sonorasoft.giftshop.R;
import com.sonorasoft.giftshop.databinding.ActivityRegisterBinding;
import com.sonorasoft.giftshop.models.LoginResponse;
import com.sonorasoft.giftshop.receivers.NetworkStatusReceiver;
import com.sonorasoft.giftshop.receivers.RegisterReceiver;
import com.sonorasoft.giftshop.services.CallService;
import com.sonorasoft.giftshop.services.LoginService;
import com.sonorasoft.giftshop.services.RegisterService;
import com.sonorasoft.giftshop.utils.Helpers;
import com.sonorasoft.giftshop.utils.Utils;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class RegisterActivity extends AppCompatActivity implements
        NetworkStatusReceiver.Callback,
        RegisterReceiver.Callback {

    ActivityRegisterBinding binding;

    private NetworkStatusReceiver networkStatusReceiver;
    private RegisterReceiver registerReceiver;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        binding = DataBindingUtil.setContentView(this, R.layout.activity_register);

        this.registerReceiver = new RegisterReceiver(this);
        this.networkStatusReceiver = new NetworkStatusReceiver(this);

        this.registerReceiver.register(this);

        setTitle("Registro");
        setSupportActionBar(binding.toolbar);
        getSupportActionBar().setTitle("Registro");

        binding.btnRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(validateFields()){
                    requestRegister();
                }
            }
        });
    }

    private void requestRegister() {
        showLoading(true);

        String email = binding.edEmail.getText().toString().trim();
        String password = binding.edContrasenia.getText().toString();
        String phone = binding.edPhone.getText().toString().trim();
        String name = binding.edName.getText().toString().trim();

        CallService.execute(this, RegisterService.getInstanceService(this, name, email, password, phone));
    }

    private boolean validateFields() {

        String nombre = binding.edName.getText().toString().trim();
        if(nombre.isEmpty()){
            Utils.showDialogError(this, "Error", "Ingrese un nombre");
            return false;
        }

        String correo = binding.edEmail.getText().toString().trim();
        if(correo.isEmpty()){
            Utils.showDialogError(this, "Error", "Ingrese un correo electrónico");
            return false;
        }

        if(!Helpers.validateEmailFormat(correo)){
            Utils.showDialogError(this, "Error", "Ingrese un correo con formato válido");
            return false;
        }

        String celular = binding.edPhone.getText().toString().trim();
        if(celular.isEmpty()){
            Utils.showDialogError(this, "Error", "Ingrese un celular");
            return false;
        }

        if(celular.length() != 10){
            Utils.showDialogError(this, "Error", "Ingrese un número celular válido de 10 dígitos");
            return false;
        }

        String contrasenia = binding.edContrasenia.getText().toString();
        if(contrasenia.isEmpty()){
            Utils.showDialogError(this, "Error", "Ingrese una contraseña");
            return false;
        }

        return true;
    }

    @Override
    public void onDestroy() {
        this.registerReceiver.unregister(this);
        super.onDestroy();
    }

    @Override
    public void onResume() {
        super.onResume();
        networkStatusReceiver.register(this);
    }

    @Override
    public void onPause() {
        networkStatusReceiver.unregister(this);
        super.onPause();
    }

    @Override
    public void onNetworkStatusNoInternet() {
        showLoading(false);
        Snackbar.make(binding.getRoot(), "Revise que tenga una conexión activa a internet", Snackbar.LENGTH_LONG).show();
    }

    @Override
    public void onNetworkStatusFailComunicationWs() {
        showLoading(false);
        Snackbar.make(binding.getRoot(), "Error de comunicación con el servidor", Snackbar.LENGTH_LONG).show();
    }

    @Override
    public void onRegisterSuccess(LoginResponse loginResponse) {
        showLoading(false);
        Utils.showDialogAndClose(this, "Registro éxito", "Su nueva cuenta ha sido generada y activada");
    }

    @Override
    public void onRegisterFailure(String message, int code) {
        showLoading(false);
        Utils.showDialogError(this, "Error al iniciar sesión", message);
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    private void showLoading(final boolean show){
        int shortAnimTime = getResources().getInteger(
                android.R.integer.config_mediumAnimTime);

        binding.registerStatus.animate().setDuration(shortAnimTime)
                .alpha(show ? 1 : 0)
                .setListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        binding.registerStatus.setVisibility(show ? View.VISIBLE
                                : View.GONE);
                    }
                });

        binding.mainContainRegister.animate().setDuration(shortAnimTime)
                .alpha(show ? 0 : 1)
                .setListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        binding.mainContainRegister.setVisibility(show ? View.GONE
                                : View.VISIBLE);
                    }
                });
    }
}
