package com.sonorasoft.giftshop.database;

import com.sonorasoft.giftshop.GiftShopApplication;
import com.sonorasoft.giftshop.models.LoginResponse;

import io.realm.Realm;

/**
 * Created by julionava on 11/25/17.
 */

public class Database {

    public static void deleteLogin(){
        Realm realm = GiftShopApplication.getRealmInstance();
        realm.beginTransaction();
        realm.delete(LoginResponse.class);
        realm.commitTransaction();
    }

    public static void saveLogin(LoginResponse loginResponse){
        Realm realm = GiftShopApplication.getRealmInstance();
        realm.beginTransaction();
        loginResponse.setIdKey(GiftShopApplication.ID_SESSION_LOGIN);
        realm.copyToRealmOrUpdate(loginResponse);
        realm.commitTransaction();
    }

    public static LoginResponse getCurrentSession(){
        return GiftShopApplication.getRealmInstance().where(LoginResponse.class).equalTo("idKey", GiftShopApplication.ID_SESSION_LOGIN).findFirst();
    }

    public static int getIdCurrentUser(){
        int idCurrentUser = 0;
        if(getCurrentSession() != null)
            idCurrentUser = getCurrentSession().getId();
        return idCurrentUser;
    }

}
