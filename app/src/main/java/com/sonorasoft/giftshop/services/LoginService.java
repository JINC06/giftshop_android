package com.sonorasoft.giftshop.services;

import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.Nullable;

import com.sonorasoft.giftshop.controllers.ApiController;
import com.sonorasoft.giftshop.models.LoginResponse;
import com.sonorasoft.giftshop.models.ServerResponse;
import com.sonorasoft.giftshop.receivers.LoginReceiver;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by julionava on 11/25/17.
 */

public class LoginService extends IntentService {

    public static final String TAG = LoginService.class.getSimpleName();
    public static final String EMAIL = "com.sonorasoft.giftshop.EMAIL";
    public static final String PASSWORD = "com.sonorasoft.giftshop.PASSWORD";

    public static void startService(Context context, String email, String password) {
        Intent intent = new Intent(context, LoginService.class);
        intent.putExtra(EMAIL, email);
        intent.putExtra(PASSWORD, password);
        context.startService(intent);
    }

    public static Intent getInstanceService(Context context, String email, String password){
        Intent intent = new Intent(context, LoginService.class);
        intent.putExtra(EMAIL, email);
        intent.putExtra(PASSWORD, password);
        return intent;
    }

    public LoginService() {
        super(TAG);
    }

    @Override
    protected void onHandleIntent(@Nullable Intent intent) {


        String email = intent.getStringExtra(EMAIL);
        String password = intent.getStringExtra(PASSWORD);

        Call<LoginResponse> callResponse = ApiController.getApi().login(email, password);
        callResponse.enqueue(new Callback<LoginResponse>() {

            LoginService $this = LoginService.this;

            @Override
            public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {

                if(response.isSuccessful()){
                    LoginResponse loginResponse = response.body();
                    LoginReceiver.success($this, loginResponse);
                }else {
                    int code = response.code();
                    String message = "Error de comunicación con el servidor";
                    try{
                        String json = response.errorBody().string();
                        ServerResponse modelError = ApiController.getGson().fromJson(json, ServerResponse.class);
                        message = modelError.getMessage();
                    }catch (Exception e){}

                    LoginReceiver.failure($this, message, code);
                }
            }

            @Override
            public void onFailure(Call<LoginResponse> call, Throwable t) {
                LoginReceiver.failure($this, "Error de comunicación con el servidor", 0);
            }
        });


    }

}
