package com.sonorasoft.giftshop.services;

import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.Nullable;

import com.sonorasoft.giftshop.controllers.ApiController;
import com.sonorasoft.giftshop.models.LoginResponse;
import com.sonorasoft.giftshop.models.ServerResponse;
import com.sonorasoft.giftshop.receivers.RegisterReceiver;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by julionava on 11/25/17.
 */

public class RegisterService extends IntentService {

    public static final String TAG = RegisterService.class.getSimpleName();
    public static final String EMAIL = "com.sonorasoft.giftshop.EMAIL";
    public static final String PASSWORD = "com.sonorasoft.giftshop.PASSWORD";
    public static final String PHONE = "com.sonorasoft.giftshop.PHONE";
    public static final String NAME = "com.sonorasoft.giftshop.NAME";

    public static void startService(Context context, String name, String email, String password, String phone) {
        Intent intent = new Intent(context, RegisterService.class);
        intent.putExtra(EMAIL, email);
        intent.putExtra(PASSWORD, password);
        intent.putExtra(PHONE, phone);
        intent.putExtra(NAME, name);
        context.startService(intent);
    }

    public static Intent getInstanceService(Context context, String name, String email, String password, String phone){
        Intent intent = new Intent(context, RegisterService.class);
        intent.putExtra(EMAIL, email);
        intent.putExtra(PASSWORD, password);
        intent.putExtra(PHONE, phone);
        intent.putExtra(NAME, name);
        return intent;
    }

    public RegisterService() {
        super(TAG);
    }

    @Override
    protected void onHandleIntent(@Nullable Intent intent) {


        String email = intent.getStringExtra(EMAIL);
        String password = intent.getStringExtra(PASSWORD);
        String name = intent.getStringExtra(NAME);
        String phone = intent.getStringExtra(PHONE);

        Call<LoginResponse> callResponse = ApiController.getApi().register(name, email, password, phone);
        callResponse.enqueue(new Callback<LoginResponse>() {

            RegisterService $this = RegisterService.this;
            @Override
            public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {

                if(response.isSuccessful()){
                    LoginResponse loginResponse = response.body();
                    RegisterReceiver.success($this, loginResponse);
                }else {
                    int code = response.code();
                    String message = "Error de comunicación con el servidor";
                    try{
                        String json = response.errorBody().string();
                        ServerResponse modelError = ApiController.getGson().fromJson(json, ServerResponse.class);
                        message = modelError.getMessage();
                    }catch (Exception e){}

                    RegisterReceiver.failure($this, message, code);
                }

            }

            @Override
            public void onFailure(Call<LoginResponse> call, Throwable t) {
                RegisterReceiver.failure($this, "Error de comunicación con el servidor", 0);
            }
        });

    }

}
