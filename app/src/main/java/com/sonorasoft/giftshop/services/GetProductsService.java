package com.sonorasoft.giftshop.services;

import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.Nullable;

import com.sonorasoft.giftshop.controllers.ApiController;
import com.sonorasoft.giftshop.models.GetProductsResponse;
import com.sonorasoft.giftshop.receivers.GetCategoriesReceiver;
import com.sonorasoft.giftshop.receivers.GetProductsReceiver;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by julionava on 11/25/17.
 */

public class GetProductsService extends IntentService {

    public static final String TAG = GetProductsService.class.getSimpleName();
    public static final String IDCATEGORY = "com.sonorasoft.giftshop.IDCATEGORY";
    public static final String SEARCH_QUERY = "com.sonorasoft.giftshop.SEARCH_QUERY";
    public static final String PER_PAGE = "com.sonorasoft.giftshop.PER_PAGE";
    public static final String CURRENT_PAGE = "com.sonorasoft.giftshop.CURRENT_PAGE";


    public static Intent getInstanceService(Context context, int idCategory, String searchQuery, int perPage, int currentPage){
        Intent intent = new Intent(context, GetProductsService.class);
        intent.putExtra(IDCATEGORY, idCategory);
        intent.putExtra(SEARCH_QUERY, searchQuery);
        intent.putExtra(PER_PAGE, perPage);
        intent.putExtra(CURRENT_PAGE, currentPage);
        return intent;
    }

    public GetProductsService() {
        super(TAG);
    }

    @Override
    protected void onHandleIntent(@Nullable Intent intent) {

        int idCategory = intent.getIntExtra(IDCATEGORY, 0);
        String searchQuery = intent.getStringExtra(SEARCH_QUERY);
        int perPage = intent.getIntExtra(PER_PAGE, 5);
        int currentPage = intent.getIntExtra(CURRENT_PAGE, 1);

        Call<GetProductsResponse> callResponse = ApiController.getApi().getProducts(idCategory, searchQuery, perPage, currentPage);
        callResponse.enqueue(new Callback<GetProductsResponse>() {
            GetProductsService $this = GetProductsService.this;

            @Override
            public void onResponse(Call<GetProductsResponse> call, Response<GetProductsResponse> response) {
                if(response.isSuccessful()){
                    GetProductsReceiver.success($this, response.body());
                }else{
                    GetProductsReceiver.failure($this, "Error de comunicación con el servidor", 0);
                }
            }

            @Override
            public void onFailure(Call<GetProductsResponse> call, Throwable t) {
                GetProductsReceiver.failure($this, "Error de comunicación con el servidor", 0);
            }
        });

    }

}
