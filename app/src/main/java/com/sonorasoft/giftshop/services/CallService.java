package com.sonorasoft.giftshop.services;

import android.content.Context;
import android.content.Intent;

import com.sonorasoft.giftshop.receivers.NetworkStatusReceiver;
import com.sonorasoft.giftshop.utils.Helpers;

/**
 * Created by julionava on 11/25/17.
 */

public class CallService {


    public static void execute(final Context context, final Intent service)
    {

        if(Helpers.isNetworkConnected(context)) {

            Thread thread = new Thread() {
                @Override
                public void run() {
                    try {

                        if(Helpers.isWebServicesAvailable()) {

                            context.startService(service);

                        }else{
                            NetworkStatusReceiver.failureFailComunicationWs(context);
                        }


                    } catch (Exception e) {
                        NetworkStatusReceiver.failureNoInternet(context);
                    }
                }
            };
            thread.start();

        }else{
            NetworkStatusReceiver.failureNoInternet(context);
        }

    }

}
