package com.sonorasoft.giftshop.services;

import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.Nullable;

import com.sonorasoft.giftshop.controllers.ApiController;
import com.sonorasoft.giftshop.models.Category;
import com.sonorasoft.giftshop.receivers.GetCategoriesReceiver;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by julionava on 11/25/17.
 */

public class GetCategoriesService extends IntentService {

    public static final String TAG = GetCategoriesService.class.getSimpleName();

    public static Intent getInstanceService(Context context){
        Intent intent = new Intent(context, GetCategoriesService.class);
        return intent;
    }

    public GetCategoriesService() {
        super(TAG);
    }

    @Override
    protected void onHandleIntent(@Nullable Intent intent) {
        Call<List<Category>> callResponse = ApiController.getApi().getCategories();
        callResponse.enqueue(new Callback<List<Category>>() {

            GetCategoriesService $this = GetCategoriesService.this;

            @Override
            public void onResponse(Call<List<Category>> call, Response<List<Category>> response) {

                if(response.isSuccessful()){
                    List<Category> categories = response.body();
                    GetCategoriesReceiver.success($this, categories);
                }else{
                    GetCategoriesReceiver.failure($this, "Error de comunicación con el servidor", 0);
                }

            }

            @Override
            public void onFailure(Call<List<Category>> call, Throwable t) {
                GetCategoriesReceiver.failure($this, "Error de comunicación con el servidor", 0);
            }
        });
    }

}
