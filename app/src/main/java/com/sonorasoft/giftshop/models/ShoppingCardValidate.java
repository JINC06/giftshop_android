package com.sonorasoft.giftshop.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by julionava on 11/24/17.
 */

public class ShoppingCardValidate implements Serializable {

    @SerializedName("products")
    @Expose
    private List<Product> products = new ArrayList<>();
    @SerializedName("total")
    @Expose
    private Double total;

    public List<Product> getProducts() {
        return products;
    }

    public void setProducts(List<Product> products) {
        this.products = products;
    }

    public Double getTotal() {
        return total;
    }

    public void setTotal(Double total) {
        this.total = total;
    }
}
