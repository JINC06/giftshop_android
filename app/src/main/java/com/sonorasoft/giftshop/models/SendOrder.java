package com.sonorasoft.giftshop.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by julionava on 11/25/17.
 */

public class SendOrder implements Serializable {

    @SerializedName("products")
    @Expose
    private List<Product> products = new ArrayList<>();
    @SerializedName("total")
    @Expose
    private Double total;
    @SerializedName("id_user")
    @Expose
    private Integer idUser;
    @SerializedName("cardname_holder")
    @Expose
    private String cardnameHolder;
    @SerializedName("card_number")
    @Expose
    private String cardNumber;
    @SerializedName("card_type")
    @Expose
    private String cardType;
    @SerializedName("expiration_date")
    @Expose
    private String expirationDate;
    @SerializedName("ccv")
    @Expose
    private String ccv;
    @SerializedName("code")
    @Expose
    private String code;

    public List<Product> getProducts() {
        return products;
    }

    public void setProducts(List<Product> products) {
        this.products = products;
    }

    public Double getTotal() {
        return total;
    }

    public void setTotal(Double total) {
        this.total = total;
    }

    public Integer getIdUser() {
        return idUser;
    }

    public void setIdUser(Integer idUser) {
        this.idUser = idUser;
    }

    public String getCardnameHolder() {
        return cardnameHolder;
    }

    public void setCardnameHolder(String cardnameHolder) {
        this.cardnameHolder = cardnameHolder;
    }

    public String getCardNumber() {
        return cardNumber;
    }

    public void setCardNumber(String cardNumber) {
        this.cardNumber = cardNumber;
    }

    public String getCardType() {
        return cardType;
    }

    public void setCardType(String cardType) {
        this.cardType = cardType;
    }

    public String getExpirationDate() {
        return expirationDate;
    }

    public void setExpirationDate(String expirationDate) {
        this.expirationDate = expirationDate;
    }

    public String getCcv() {
        return ccv;
    }

    public void setCcv(String ccv) {
        this.ccv = ccv;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
}
