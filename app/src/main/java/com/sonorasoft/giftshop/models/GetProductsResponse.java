package com.sonorasoft.giftshop.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by julionava on 11/24/17.
 */

public class GetProductsResponse implements Serializable {


    @SerializedName("total_results")
    @Expose
    private String totalResults;
    @SerializedName("pages")
    @Expose
    private String pages;
    @SerializedName("current_page")
    @Expose
    private String currentPage;
    @SerializedName("per_page")
    @Expose
    private String perPage;
    @SerializedName("results")
    @Expose
    private List<Product> results = new ArrayList<>();

    public String getTotalResults() {
        return totalResults;
    }

    public void setTotalResults(String totalResults) {
        this.totalResults = totalResults;
    }

    public String getPages() {
        return pages;
    }

    public void setPages(String pages) {
        this.pages = pages;
    }

    public String getCurrentPage() {
        return currentPage;
    }

    public void setCurrentPage(String currentPage) {
        this.currentPage = currentPage;
    }

    public String getPerPage() {
        return perPage;
    }

    public void setPerPage(String perPage) {
        this.perPage = perPage;
    }

    public List<Product> getResults() {
        return results;
    }

    public void setResults(List<Product> results) {
        this.results = results;
    }
}
