package com.sonorasoft.giftshop.fragments.dialogs;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;

import com.sonorasoft.giftshop.R;
import com.sonorasoft.giftshop.activities.MainActivity;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link FilterDialogFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link FilterDialogFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class FilterDialogFragment extends DialogFragment {

    public static final String TAG = FilterDialogFragment.class.getSimpleName();
    private OnFragmentInteractionListener mListener;

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment FilterDialogFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static FilterDialogFragment newInstance() {
        FilterDialogFragment fragment = new FilterDialogFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }


    EditText edSearch;
    Spinner spCategories;
    Spinner spPerPage;

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        LayoutInflater inflater = LayoutInflater.from(this.getActivity());
        View content = inflater.inflate(R.layout.fragment_filter_dialog, null);

        edSearch = content.findViewById(R.id.edSearch);
        spCategories = content.findViewById(R.id.spCategories);
        spPerPage = content.findViewById(R.id.spResultsPerPage);

        edSearch.setText(MainActivity.searchQuery);
        populateSpinnerCategories();
        spCategories.setSelection(positionInitialCategory);
        populateSpinnerPerPage();
        spPerPage.setSelection(positionInitialPerPage);

        return new AlertDialog.Builder(getActivity())
                .setCancelable(false)
                .setView(content)
                .setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                })
                .setPositiveButton("Buscar", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        MainActivity.searchQuery = edSearch.getText().toString().trim();
                        MainActivity.idCategory = MainActivity.categoryList.get(spCategories.getSelectedItemPosition()).getId();
                        MainActivity.perPage = Integer.parseInt(MainActivity.valuesPerPages[spPerPage.getSelectedItemPosition()]);
                        MainActivity.currentPage = 1;
                        if(mListener != null){
                            mListener.searchCriteria();
                        }

                        dialog.dismiss();
                    }
                })
                .create();
    }

    int positionInitialCategory = 0;
    private void populateSpinnerCategories() {
        List<String> lables = new ArrayList<String>();

        for (int i = 0; i < MainActivity.categoryList.size(); i++) {
            lables.add(MainActivity.categoryList.get(i).getDescription());
            if(MainActivity.categoryList.get(i).getId() == MainActivity.idCategory){
                positionInitialCategory = i;
            }
        }

        // Creating adapter for spinner
        ArrayAdapter<String> spinnerAdapter = new ArrayAdapter<String>(getActivity(),
                android.R.layout.simple_spinner_item, lables);

        // Drop down layout style - list view with radio button
        spinnerAdapter
                .setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        // attaching data adapter to spinner
        spCategories.setAdapter(spinnerAdapter);
    }

    int positionInitialPerPage = 0;
    private void populateSpinnerPerPage() {
        List<String> lables = new ArrayList<String>();

        for (int i = 0; i < MainActivity.valuesPerPages.length; i++) {
            lables.add(MainActivity.valuesPerPages[i]);
            if(Integer.parseInt(MainActivity.valuesPerPages[i]) == MainActivity.perPage){
                positionInitialPerPage = i;
            }
        }

        // Creating adapter for spinner
        ArrayAdapter<String> spinnerAdapter = new ArrayAdapter<String>(getActivity(),
                android.R.layout.simple_spinner_item, lables);

        // Drop down layout style - list view with radio button
        spinnerAdapter
                .setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        // attaching data adapter to spinner
        spPerPage.setAdapter(spinnerAdapter);
    }




    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        //super.onViewCreated(view, savedInstanceState);
        this.setStyle(DialogFragment.STYLE_NO_TITLE, 0);
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
        void searchCriteria();
    }

    @Override
    public void onDismiss(DialogInterface dialog) {
        super.onDismiss(dialog);
        Fragment parentFragment = getParentFragment();
        if(parentFragment instanceof DialogInterface.OnDismissListener){
            ((DialogInterface.OnDismissListener) parentFragment).onDismiss(dialog);
        }
    }

    public OnFragmentInteractionListener getmListener() {
        return mListener;
    }

    public void setmListener(OnFragmentInteractionListener mListener) {
        this.mListener = mListener;
    }
}
