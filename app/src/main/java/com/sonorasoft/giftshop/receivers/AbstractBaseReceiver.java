package com.sonorasoft.giftshop.receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.IntentFilter;
import android.support.annotation.NonNull;
import android.support.v4.content.LocalBroadcastManager;

/**
 * Created by julionava on 11/25/17.
 */

public abstract class AbstractBaseReceiver extends BroadcastReceiver {

    public static final String TAG = AbstractBaseReceiver.class.getSimpleName();
    private final boolean mGlobal;

    public AbstractBaseReceiver(boolean global) {
        this.mGlobal = global;
    }

    public final void register(@NonNull Context context) {
        String filters[] = this.registerFilters();
        for (String filter : filters) {
            IntentFilter intentFilter = new IntentFilter(filter);
            if (this.mGlobal) {
                context.registerReceiver(this, intentFilter);
            } else {
                LocalBroadcastManager.getInstance(context).registerReceiver(this, intentFilter);
            }
        }
    }

    public final void unregister(@NonNull Context context) {
        if (this.mGlobal) {
            context.unregisterReceiver(this);
        } else {
            LocalBroadcastManager.getInstance(context).unregisterReceiver(this);
        }
    }

    protected abstract String[] registerFilters();

}
