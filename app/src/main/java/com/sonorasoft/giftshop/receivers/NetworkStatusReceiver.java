package com.sonorasoft.giftshop.receivers;

import android.content.Context;
import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;
import android.text.TextUtils;
import android.util.Log;

import java.lang.ref.WeakReference;

/**
 * Created by julionava on 11/25/17.
 */

public class NetworkStatusReceiver extends AbstractBaseReceiver {


    public static final String TAG = NetworkStatusReceiver.class.getSimpleName();
    public static final String BROADCAST_ACTION_NETWORK_NO_INTERNET = "com.sonorasoft.giftshop.BROADCAST_ACTION_NETWORK_NO_INTERNET";
    public static final String BROADCAST_ACTION_NETWORK_FAIL_COMUNICATION_WS = "com.sonorasoft.giftshop.BROADCAST_ACTION_NETWORK_FAIL_WS";

    public static void failureNoInternet(Context context) {
        Intent intent = new Intent(BROADCAST_ACTION_NETWORK_NO_INTERNET);
        LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
    }

    public static void failureFailComunicationWs(Context context) {
        Intent intent = new Intent(BROADCAST_ACTION_NETWORK_FAIL_COMUNICATION_WS);
        LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
    }

    public interface Callback {
        void onNetworkStatusNoInternet();
        void onNetworkStatusFailComunicationWs();
    }

    private final WeakReference<Callback> mWeakCallback;

    public NetworkStatusReceiver(Callback callback) {
        super(false);
        this.mWeakCallback = new WeakReference<>(callback);
    }

    @Override
    protected String[] registerFilters() {
        return new String[]{
                BROADCAST_ACTION_NETWORK_NO_INTERNET,
                BROADCAST_ACTION_NETWORK_FAIL_COMUNICATION_WS
        };
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        Callback callback = this.mWeakCallback.get();
        if (callback == null) {
            Log.w(TAG, "Callback expired!");
            return;
        }
        if (TextUtils.equals(BROADCAST_ACTION_NETWORK_NO_INTERNET, intent.getAction())) {
            callback.onNetworkStatusNoInternet();
        }
        if (TextUtils.equals(BROADCAST_ACTION_NETWORK_FAIL_COMUNICATION_WS, intent.getAction())) {
            callback.onNetworkStatusFailComunicationWs();
        }
    }

}
