package com.sonorasoft.giftshop.receivers;

import android.content.Context;
import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;
import android.text.TextUtils;
import android.util.Log;

import com.sonorasoft.giftshop.models.Category;

import java.io.Serializable;
import java.lang.ref.WeakReference;
import java.util.List;

/**
 * Created by julionava on 11/25/17.
 */

public class GetCategoriesReceiver extends AbstractBaseReceiver {

    public static final String TAG = GetCategoriesReceiver.class.getSimpleName();
    public static final String BROADCAST_ACTION_SERVICE_SUCCESS = "com.sonorasoft.giftshop.BROADCAST_GET_CATEGORIES_SUCCESS";
    public static final String BROADCAST_ACTION_SERVICE_FAILURE = "com.sonorasoft.giftshop.BROADCAST_GET_CATEGORIES_FAILURE";
    public static final String PARAM_MESSAGE = "PARAM_STATUS_MESSAGE";
    public static final String CODE = "CODE";
    public static final String PARAM_CATEGORIES = "PARAM_CATEGORIES";

    public static void success(Context context, List<Category> categories) {
        Intent intent = new Intent(BROADCAST_ACTION_SERVICE_SUCCESS);
        intent.putExtra(PARAM_CATEGORIES, (Serializable) categories);
        LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
    }

    public static void failure(Context context, String message, int code) {
        Intent intent = new Intent(BROADCAST_ACTION_SERVICE_FAILURE);
        intent.putExtra(PARAM_MESSAGE, message);
        intent.putExtra(CODE, code);

        LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
    }

    public interface Callback {
        void onGetCategoriesSuccess(List<Category> categories);
        void onGetCategoriesFailure(String message, int code);
    }

    private final WeakReference<Callback> mWeakCallback;

    public GetCategoriesReceiver(Callback callback) {
        super(false);
        this.mWeakCallback = new WeakReference<>(callback);
    }

    @Override
    protected String[] registerFilters() {
        return new String[]{
                BROADCAST_ACTION_SERVICE_SUCCESS,
                BROADCAST_ACTION_SERVICE_FAILURE
        };
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        Callback callback = this.mWeakCallback.get();
        if (callback == null) {
            Log.w(TAG, "Callback expired!");
            return;
        }
        if (TextUtils.equals(BROADCAST_ACTION_SERVICE_SUCCESS, intent.getAction())) {
            List<Category> categories = (List<Category>) intent.getSerializableExtra(PARAM_CATEGORIES);
            callback.onGetCategoriesSuccess(categories);
        }
        if (TextUtils.equals(BROADCAST_ACTION_SERVICE_FAILURE, intent.getAction())) {
            String message = intent.getStringExtra(PARAM_MESSAGE);
            int code = intent.getIntExtra(CODE, 0);
            callback.onGetCategoriesFailure(message, code);
        }
    }

}
