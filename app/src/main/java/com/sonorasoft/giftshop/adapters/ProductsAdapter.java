package com.sonorasoft.giftshop.adapters;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.sonorasoft.giftshop.databinding.ItemProductBinding;
import com.sonorasoft.giftshop.models.Product;

import java.util.List;

/**
 * Created by julionava on 11/25/17.
 */

public class ProductsAdapter extends RecyclerView.Adapter<ProductsAdapter.ProductViewHolder> {

    private List<Product> data;
    private Context context;
    private LayoutInflater layoutInflater;

    public List<Product> getData() {
        return data;
    }

    public void setData(List<Product> data) {
        this.data = data;
    }

    public Context getContext() {
        return context;
    }

    public void setContext(Context context) {
        this.context = context;
    }

    public LayoutInflater getLayoutInflater() {
        return layoutInflater;
    }

    public void setLayoutInflater(LayoutInflater layoutInflater) {
        this.layoutInflater = layoutInflater;
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public ProductsAdapter(LayoutInflater layoutInflater, List<Product> data, Context context){
        this.layoutInflater = layoutInflater;
        this.data = data;
        this.context = context;
    }

    @Override
    public ProductViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        ItemProductBinding binding = ItemProductBinding.inflate(layoutInflater, parent, false);
        return new ProductViewHolder(binding.getRoot());
    }

    @Override
    public void onBindViewHolder(ProductViewHolder holder, int position) {
        final ItemProductBinding binding = DataBindingUtil.getBinding(holder.itemView);
        final Product product = data.get(position);

        binding.productName.setText( product.getName() );
        binding.productDescription.setText( product.getDescription() );
        binding.productPrice.setText("Precio: $" + product.getPrice());
        if(context != null && product.getImage() != null){
            Glide.with(context).load(product.getImage()).into(binding.productImage);
        }

        binding.btnViewProduct.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(context, "Ver el detalle del producto " + product.getName(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    static class ProductViewHolder extends RecyclerView.ViewHolder {

        public ProductViewHolder(View itemView){
            super(itemView);
        }

    }

}
