package com.sonorasoft.giftshop;

import android.app.Application;
import android.content.Context;

import com.sonorasoft.giftshop.controllers.ApiController;
import com.sonorasoft.giftshop.database.Database;
import com.sonorasoft.giftshop.utils.SharedPreferenceHelper;

import io.realm.Realm;
import io.realm.RealmConfiguration;
import io.realm.exceptions.RealmMigrationNeededException;
import uk.co.chrisjenx.calligraphy.CalligraphyConfig;

/**
 * Created by julionava on 11/24/17.
 */

public class GiftShopApplication extends Application {

    public static final String TAG = GiftShopApplication.class.getSimpleName();

    //PRODUCCION
    //public static final String BASE_URL_SERVICES = "http://giftshop.pitalla.mx/";
    //public static final String DOMAIN = "giftshop.pitalla.mx";



    //PRUEBA
    public static String BASE_URL_SERVICES = "http://192.168.1.70/";
    public static String DOMAIN = "192.168.1.70";

    private static GiftShopApplication instance;
    private static SharedPreferenceHelper sharedPreferenceHelper;
    private static RealmConfiguration realmConfig;
    public static final String GIFTSHOP_PREFERENCES = "GIFTSHOP_PREFERENCES";
    public static final int ID_SESSION_LOGIN = 0xcafe;
    public static final String PREFERENCE_IS_USER_ACTIVE = "PREFERENCE_IS_USER_ACTIVE";

    @Override
    public void onCreate() {
        super.onCreate();

        instance = this;
        sharedPreferenceHelper = new SharedPreferenceHelper(GIFTSHOP_PREFERENCES ,this);

        ApiController.init(BASE_URL_SERVICES);

        //Init realm
        Realm.init(this);
        realmConfig = new RealmConfiguration.Builder().build();
        Realm.setDefaultConfiguration(realmConfig);


        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath("fonts/Lato-Regular.ttf")
                .setFontAttrId(R.attr.fontPath)
                .build()
        );

    }

    @Override
    protected void attachBaseContext(Context context) {
        super.attachBaseContext(context);
    }

    public static synchronized Realm getRealmInstance() {
        try {
            return Realm.getInstance(realmConfig);
        } catch (RealmMigrationNeededException e){
            try {
                Realm.deleteRealm(realmConfig);
                //Realm file has been deleted.
                return Realm.getInstance(realmConfig);
            } catch (Exception ex){
                throw ex;
                //No Realm file to remove.
            }
        }
    }

    public static synchronized void setSessionActive(boolean sessionActive){
        sharedPreferenceHelper.writeString(PREFERENCE_IS_USER_ACTIVE, String.valueOf(sessionActive));
    }

    public static synchronized boolean isSessionActive(){
        boolean isSessionActive = false;
        String pref = sharedPreferenceHelper.getStringFromShprf(PREFERENCE_IS_USER_ACTIVE);
        if(pref != null)
            isSessionActive = Boolean.parseBoolean(pref);

        if(isSessionActive){
            if(Database.getCurrentSession() == null){
                isSessionActive = false;
            }
        }

        return isSessionActive;
    }

}
