package com.sonorasoft.giftshop.controllers;

import android.support.annotation.NonNull;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.sonorasoft.giftshop.models.Category;
import com.sonorasoft.giftshop.models.GetProductsResponse;
import com.sonorasoft.giftshop.models.LoginResponse;
import com.sonorasoft.giftshop.models.Product;
import com.sonorasoft.giftshop.models.SendOrder;
import com.sonorasoft.giftshop.models.ServerResponse;
import com.sonorasoft.giftshop.models.ShoppingCardValidate;
import com.sonorasoft.giftshop.utils.GsonUtils;

import java.util.List;
import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by julionava on 11/24/17.
 */

public class ApiController {

    public static final String TAG = ApiController.class.getSimpleName();
    private static Api sApi = null;
    private static Gson sGson = null;

    ApiController() {
        throw new RuntimeException("Don't!");
    }

    public static void init(@NonNull String baseUrl) {
        //if (sApi != null) {
        //    throw new IllegalStateException("Api already initialized");
        //}
        GsonUtils.ExcludeFieldsWithoutSerializedName efwsn = new GsonUtils.ExcludeFieldsWithoutSerializedName();
        sGson = new GsonBuilder()
                .setDateFormat("yyyy-MM-dd HH:mm:ss")
                .addSerializationExclusionStrategy(efwsn)
                .addDeserializationExclusionStrategy(efwsn)
                .registerTypeHierarchyAdapter(byte[].class, new GsonUtils.ByteArrayToBase64Serializer())
                .create();
        GsonConverterFactory gsonConverter = GsonConverterFactory.create(sGson);

        OkHttpClient okHttpClient = new OkHttpClient().newBuilder()
                .connectTimeout(30, TimeUnit.SECONDS)
                .readTimeout(30, TimeUnit.SECONDS)
                .writeTimeout(30, TimeUnit.SECONDS)
                .build();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(baseUrl)
                .client(okHttpClient)
                .addConverterFactory(gsonConverter)
                .build();

        sApi = retrofit.create(Api.class);
    }

    public static Api getApi() {
        return sApi;
    }

    public static Gson getGson() {
        return sGson;
    }


    public interface Api {

        @FormUrlEncoded
        @POST("giftshop/v1/login/")
        //@POST("v1/login/")
        Call<LoginResponse> login(
                @Field("email") String email,
                @Field("password") String password
        );

        @FormUrlEncoded
        @POST("giftshop/v1/user/")
        //@POST("v1/user/")
        Call<LoginResponse> register(
                @Field("name") String name,
                @Field("email") String email,
                @Field("password") String password,
                @Field("phone") String phone
        );

        @GET("giftshop//v1/category/")
        //@GET("/v1/category/")
        Call<List<Category>> getCategories();

        @GET("giftshop/v1/product/")
        //@GET("v1/product/")
        Call<GetProductsResponse> getProducts(
                @Query("id_category") Integer idCategory,
                @Query("search_query") String searchQuery,
                @Query("per_page") Integer perPage,
                @Query("current_page") Integer currentPage
        );

        @FormUrlEncoded
        @POST("giftshop/v1/product/")
        //@POST("v1/product/")
        Call<Product> newProduct(
                @Field("name") String name,
                @Field("price") Double price,
                @Field("description") String description,
                @Field("sku") String sku,
                @Field("category") Integer category,
                @Field("image") String image
        );

        @DELETE("giftshop/v1/product/")
        //@DELETE("v1/product/")
        Call<ServerResponse> deleteProduct(
                @Query("id") Integer id
        );

        @FormUrlEncoded
        @POST("giftshop/v1/product/edit")
        //@POST("v1/product/edit")
        Call<Product> editProduct(
                @Field("id") Integer id,
                @Field("name") String name,
                @Field("price") Double price,
                @Field("description") String description,
                @Field("sku") String sku,
                @Field("category") Integer category,
                @Field("image") String image
        );

        @POST("giftshop/v1/order/validate/")
        //@POST("v1/order/validate/")
        Call<ShoppingCardValidate> validateOrder(
                @Body ShoppingCardValidate shoppingCardValidate
        );


        @POST("giftshop/v1/order/")
        //@POST("v1/order/")
        Call<SendOrder> sendOrder(
                @Body SendOrder order
        );

    }

}
